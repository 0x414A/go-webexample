var MINI = require('minified');
var _=MINI._, $=MINI.$, $$=MINI.$$, EE=MINI.EE, HTML=MINI.HTML;


function buttonHandler(e)  {
  $.request('post', '/markdown', $('#myForm').values())
    .then(function success(result) {
      $('#pastey').fill(result);
    })
    .error(function(status, statusText, responseText) {
		  $('#result').fill('Got an error.');
	  });
}

$('#myButton').on('click', buttonHandler);
